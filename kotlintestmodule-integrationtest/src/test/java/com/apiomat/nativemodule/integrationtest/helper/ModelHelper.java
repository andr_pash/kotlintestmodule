/*
 * Copyright (c) 2014 All Rights Reserved, http://www.apiomat.com/
 *
 * This source is property of apiomat.com. You are not allowed to use or distribute this code without a contract
 * explicitly giving you these permissions. Usage of this code includes but is not limited to running it on a server or
 * copying parts from it.
 *
 * Apinauten GmbH, Hainstrasse 4, 04109 Leipzig, Germany
 *
 * 19.01.2018
 * phimi
 */
package com.apiomat.nativemodule.integrationtest.helper;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import com.apiomat.frontend.AbstractClientDataModel;
import com.apiomat.frontend.ApiomatRequestException;

/**
 * This class provides some helper method to work with ApiOmat data models
 * 
 * @author phimi
 */
public class ModelHelper
{
	public static String getId( final AbstractClientDataModel model )
	{
		String id = null;
		if ( model != null && model.getHref( ) != null )
		{
			id = model.getHref( ).substring( model.getHref( ).lastIndexOf( "/" ) + 1 );
		}
		return id;
	}

	public static void saveAOMModel( final AbstractClientDataModel model )
	{
		try
		{
			model.save( );
			assertNotNull( model.getHref( ) );
		}
		catch ( ApiomatRequestException e )
		{
			assertTrue( false );
		}
	}

	public static void deleteAOMModels( final List<? extends AbstractClientDataModel> models )
	{
		if ( models != null && models.size( ) > 0 )
		{
			models.stream( ).forEach( m -> deleteAOMModel( m ) );
		}
	}

	public static void deleteAOMModel( final AbstractClientDataModel model )
	{
		if ( model != null && model.getHref( ) != null )
		{
			try
			{
				model.delete( );
			}
			catch ( ApiomatRequestException e )
			{
				assertTrue( false );
			}
		}
	}
}
