package com.apiomat.nativemodule.kotlintestmodule.fakes

import com.apiomat.nativemodule.AbstractClientDataModel
import com.apiomat.nativemodule.IStaticMethods
import com.apiomat.nativemodule.Level
import com.apiomat.nativemodule.Request
import com.apiomat.nativemodule.basics.User
import com.apiomat.nativemodule.helpers.IModelHelper
import java.util.*
import java.util.function.Consumer

class FakeModelHelper : IModelHelper {
    override fun <T : User?> getUserFromRequest(p0: Class<T>?): T {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun warn(p0: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logError(p0: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logError(p0: Throwable?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T : AbstractClientDataModel<*>?> getModelByForeignId(p0: Class<T>?, p1: String?): Optional<T> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun log(p0: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun log(p0: Level?, p1: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T : AbstractClientDataModel<*>?> getModels(p0: Class<T>?, p1: String?): MutableList<T> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T : AbstractClientDataModel<*>?> createModel(p0: Class<T>?): T {
        val newInstance = p0!!.newInstance()
        newInstance!!.setMethods(FakeModelMethods())

        return newInstance
    }

    override fun getAppName(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T : AbstractClientDataModel<*>?> getModelById(p0: Class<T>?, p1: String?): Optional<T> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T : AbstractClientDataModel<*>?> iterateAllModels(p0: Class<T>?, p1: String?, p2: Consumer<MutableList<T>>?, p3: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T : AbstractClientDataModel<*>?> iterateAllModels(p0: Class<T>?, p1: String?, p2: Consumer<MutableList<T>>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T : AbstractClientDataModel<*>?> delete(p0: T): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T : AbstractClientDataModel<*>?> save(p0: T): T {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun throwException(p0: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun fatal(p0: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getMethods(): IStaticMethods {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun error(p0: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getRequest(): Request {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun debug(p0: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T : AbstractClientDataModel<*>?> deleteByQuery(p0: Class<T>?, p1: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T : AbstractClientDataModel<*>?, Z : AbstractClientDataModel<*>?> equalFields(p0: T, p1: Z, p2: MutableList<String>?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun trace(p0: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}