package com.apiomat.nativemodule.kotlintestmodule.fakes

import com.apiomat.nativemodule.*
import java.io.InputStream
import javax.servlet.http.HttpServletRequest
import kotlin.random.Random

class FakeModelMethods : IModelMethodsBackend {
    override fun getRestrictResourceAccess(p0: IModel<*>?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllowedRolesGrantFromObject(p0: IModel<*>?): Array<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logError(p0: String?, p1: String?, p2: String?, p3: String?, p4: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logError(p0: String?, p1: String?, p2: String?, p3: Throwable?, p4: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findByForeignId(p0: String?, p1: String?, p2: String?, p3: String?, p4: Request?): IModel<*> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T : Any?> loadReferences(p0: IModel<*>?, p1: String?, p2: Class<out AbstractClientDataModel<Any>>?): MutableList<T> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun removeReference(p0: IModel<*>?, p1: String?, p2: IModel<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun loadResource(p0: IModel<*>?, p1: String?): ByteArray {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logModel(p0: String?, p1: String?, p2: String?, p3: String?, p4: Array<out IModel<*>>?, p5: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logModel(p0: String?, p1: String?, p2: String?, p3: String?, p4: IModel<*>?, p5: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logModel(p0: String?, p1: String?, p2: String?, p3: String?, p4: Any?, p5: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun log(p0: String?, p1: String?, p2: String?, p3: String?, p4: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun log(p0: Level?, p1: String?, p2: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun log(p0: Level?, p1: String?, p2: Throwable?, p3: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun log(p0: Level?, p1: String?, p2: Any?, p3: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setAllowedRolesGrant(p0: IModel<*>?, p1: Array<out String>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findByForeignIds(p0: String?, p1: MutableCollection<String>?, p2: String?, p3: String?, p4: Request?): Array<IModel<*>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun trackGAEvent(p0: String?, p1: String?, p2: String?, p3: String?, p4: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun loadReferencedData(p0: IModel<*>?, p1: String?, p2: String?, p3: TranscodingConfiguration?): DataWrapper {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllowedRolesWrite(p0: IModel<*>?): Array<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun trackPiwikCustomVars(p0: String?, p1: String?, p2: String?, p3: Int, p4: String?, p5: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun toBase64(p0: String?): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun save(p0: IModel<*>?): String {
        val fakeId = Random.nextLong().toString()
        println("'Saving' model with fake id: $fakeId")
        
        return fakeId
    }

    override fun setAllowedRolesWrite(p0: IModel<*>?, p1: Array<out String>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteReferencedData(p0: IModel<*>?, p1: String?, p2: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun toString(p0: IModel<*>?): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllowedRolesReadFromClass(p0: IModel<*>?): Array<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun throwException(p0: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun throwException(p0: Int, p1: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addReference(p0: IModel<*>?, p1: String?, p2: IModel<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findByNames(p0: String?, p1: String?, p2: String?, p3: String?, p4: Request?): Array<IModel<*>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllowedRolesGrant(p0: IModel<*>?): Array<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun verifyRequest(p0: IModel<*>?, p1: String?, p2: Request?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllowedRolesCreateFromClass(p0: IModel<*>?): Array<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun loadBackReferences(p0: IModel<*>?): MutableList<ReferenceWrapper> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(p0: String?, p1: String?, p2: String?, p3: String?, p4: Request?): IModel<*> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setAllowedRolesRead(p0: IModel<*>?, p1: Array<out String>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getRestrictResourceAccessFromObject(p0: IModel<*>?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getReferencedHrefs(p0: IModel<*>?): MutableMap<String, MutableSet<String>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getRestrictResourceAccessFromClass(p0: IModel<*>?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun callMethod(p0: IModel<*>?, p1: String?, vararg p2: Any?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun countByNames(p0: String?, p1: String?, p2: String?, p3: String?, p4: Request?): Long {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getHttpServletRequest(): HttpServletRequest {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllowedRolesRead(p0: IModel<*>?): Array<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllowedRolesWriteFromObject(p0: IModel<*>?): Array<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addReferencedData(p0: IModel<*>?, p1: String?, p2: InputStream?, p3: String?, p4: String?): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun delete(p0: IModel<*>?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun trackPiwikGoal(p0: String?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllowedRolesGrantFromClass(p0: IModel<*>?): Array<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllowedRolesWriteFromClass(p0: IModel<*>?): Array<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllowedRolesReadFromObject(p0: IModel<*>?): Array<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveResource(p0: IModel<*>?, p1: InputStream?, p2: Boolean, p3: String?, p4: String?): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getRequestingUsername(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createObject(p0: String?, p1: String?, p2: String?, p3: Request?): IModel<*> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T : Any?> loadReference(p0: IModel<*>?, p1: String?, p2: Class<out AbstractClientDataModel<Any>>?): T {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}