package com.apiomat.nativemodule.kotlintestmodule.kotlin

import com.apiomat.nativemodule.helpers.IModelHelper
import com.apiomat.nativemodule.kotlintestmodule.TestClazz
import com.apiomat.nativemodule.kotlintestmodule.TestModel
import com.apiomat.nativemodule.kotlintestmodule.fakes.FakeModelHelper
import com.apiomat.nativemodule.kotlintestmodule.services.GoogleMapsApiService
import com.apiomat.nativemodule.kotlintestmodule.services.TestService
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.Test

class TestServiceTest {

    init {
    }

    private val modelHelper: IModelHelper = spyk(FakeModelHelper())
    private val mapsApiService: GoogleMapsApiService = mockk()

    @Test
    fun makeCorrespondingTestClazzInstance() {
        val testService = TestService(modelHelper, mapsApiService)

        val testModel = TestModel()
        testModel.id = "123"


        val testClazz = modelHelper.createModel(TestClazz::class.java)

        every {
            modelHelper.createModel(TestClazz::class.java)
        } returns testClazz

        testService.makeCorrespondingTestClazzInstance(testModel)

        assertThat(testModel.id, equalTo(testClazz.foreignId))
    }


}