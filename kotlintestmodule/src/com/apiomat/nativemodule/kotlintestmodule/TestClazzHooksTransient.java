package com.apiomat.nativemodule.kotlintestmodule;

import com.apiomat.nativemodule.*;

public class TestClazzHooksTransient<T extends TestClazz>
	implements IModelHooksTransient<TestClazz>
{
	protected TestClazz model;

	@Override
	public void setCallingModel( TestClazz model )
	{
		this.model = model;
	}

	@Override
	public String doPost( TestClazz obj, Request request )
	{
		return null;
	}

	@Override
	public TestClazz doGet( String foreignId, Request request )
	{
		return null;
	}

	@Override
	public java.util.List<TestClazz> doGetAll( String query, Request request )
	{
		return null;
	}

	@Override
	public long doCountAll( String query, Request request )
	{
		return 0;
	}

	@Override
	public void doPut( TestClazz obj, Request request )
	{
	}

	@Override
	public boolean doDelete( String foreignId, Request request )
	{
		return false;
	}

	@Override
	public boolean doDeleteAll( String query, Request request )
	{
		return false;
	}

	@Override
	public String doPostData( final String attributeName, final DataWrapper dataWrapper,
		final Request request )
	{
		return null;
	}

	@Override
	public DataWrapper doGetData( final String dataId, final String attributeName,
		final TranscodingConfiguration transcodingConfig, final Request request )
	{
		return null;
	}

	@Override
	public boolean doDeleteData( final String attributeName, final String dataId, final Request request )
	{
		return false;
	}

	/*
	 * Please note: Before doPostRef gets called, doGet gets called internally,
	 * so that this.model can be populated with attribute values.
	 */
	@Override
	public void doPostRef( Object referencedObject, String referenceName, Request request )
	{
	}

	/*
	 * Please note: Before doGetRef gets called, doGet gets called internally,
	 * so that this.model can be populated with attribute values.
	 */
	@Override
	public <Z extends AbstractClientDataModel> java.util.List<Z> doGetRef( String refName,
		String query, Request request )
	{
		return null;
	}

	/*
	 * Please note: Before doDeleteRef gets called, doGet gets called internally,
	 * so that this.model can be populated with attribute values.
	 */
	@Override
	public void doDeleteRef( String refName, String refForeignId, Request request )
	{
	}
}