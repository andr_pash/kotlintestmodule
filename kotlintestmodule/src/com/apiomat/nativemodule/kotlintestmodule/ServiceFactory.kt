package com.apiomat.nativemodule.kotlintestmodule

import com.apiomat.nativemodule.Request
import com.apiomat.nativemodule.helpers.ModelHelper
import com.apiomat.nativemodule.kotlintestmodule.services.GoogleMapsApiService
import com.apiomat.nativemodule.kotlintestmodule.services.ITestService
import com.apiomat.nativemodule.kotlintestmodule.services.TestService
import okhttp3.OkHttpClient

fun mapsApiService() = GoogleMapsApiService(OkHttpClient())
fun createService(request: Request): ITestService = TestService(ModelHelper(KotlinTestModule::getMethods.invoke(), request), mapsApiService())