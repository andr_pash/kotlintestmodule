package com.apiomat.nativemodule.kotlintestmodule;

import com.apiomat.nativemodule.AbstractClientDataModel;
import com.apiomat.nativemodule.IModelHooksNonTransient;
import com.apiomat.nativemodule.Request;

import java.util.List;

/**
 * Generated class for hooks on your TestClazz data model
 */

public class TestClazzHooksNonTransient<T extends TestClazz> implements IModelHooksNonTransient<TestClazz>
{
	protected TestClazz model;

	@Override
	public void setCallingModel( TestClazz model )
	{
		this.model = model;
	}

	@Override public void beforePost( final TestClazz obj, final Request request )
	{

	}

	@Override public void afterPost( final TestClazz obj, final Request request )
	{

	}

	@Override public boolean beforePut( final TestClazz objFromDb, final TestClazz obj, final Request request )
	{
		return false;
	}

	@Override public void afterPut( final TestClazz obj, final Request request )
	{

	}

	@Override public boolean beforeDelete( final TestClazz obj, final Request request )
	{
		return false;
	}

	@Override
	public List<TestClazz> afterGetAll( final List<TestClazz> list, final String query, final Request request )
	{
		return null;
	}

	@Override
	public boolean beforePostRef( final TestClazz obj, final Object referenceData, final String referenceName,
		final Request request )
	{
		return false;
	}

	@Override
	public void afterPostRef( final TestClazz obj, final Object referenceData, final String referenceName,
		final Request request )
	{

	}

	@Override
	public boolean beforeDeleteRef( final TestClazz obj, final Object referenceData, final String referenceName,
		final Request request )
	{
		return false;
	}

	@Override
	public void afterDeleteRef( final TestClazz obj, final Object referenceData, final String referenceName,
		final Request request )
	{

	}

	@Override
	public <Z extends AbstractClientDataModel> List<Z> afterGetAllReferences(
		final List<Z> list,
		final String query,
		final String referencename,
		final Request request )
	{
		return null;
	}
}
