package com.apiomat.nativemodule.kotlintestmodule;

import com.apiomat.nativemodule.AbstractClientDataModel;
import com.apiomat.nativemodule.IModelHooksNonTransient;
import com.apiomat.nativemodule.Request;
import com.apiomat.nativemodule.kotlintestmodule.services.ITestService;

import java.util.List;

import static com.apiomat.nativemodule.kotlintestmodule.ServiceFactoryKt.createService;

/**
 * Generated class for hooks on your TestClazz data model
 */

public class TestModelHooksNonTransient<T extends TestModel> implements IModelHooksNonTransient<TestModel>
{
	protected TestModel model;

	@Override
	public void setCallingModel( TestModel model )
	{
		this.model = model;
	}

	@Override public void beforePost( final TestModel obj, final Request request )
	{
		final ITestService service = createService( request );
		service.log( );
	}

	@Override public void afterPost( final TestModel obj, final Request request )
	{
		final ITestService service = createService( request );
		service.makeCorrespondingTestClazzInstance( obj );
		service.fetchImageForCoordinatesAndAttachToModel( obj );
		obj.log( "after post finished" );
	}

	@Override public boolean beforePut( final TestModel objFromDb, final TestModel obj, final Request request )
	{
		return false;
	}

	@Override public void afterPut( final TestModel obj, final Request request )
	{

	}

	@Override public boolean beforeDelete( final TestModel obj, final Request request )
	{
		return false;
	}

	@Override
	public List<TestModel> afterGetAll( final List<TestModel> list, final String query, final Request request )
	{
		return null;
	}

	@Override
	public boolean beforePostRef( final TestModel obj, final Object referenceData, final String referenceName,
		final Request request )
	{
		return false;
	}

	@Override
	public void afterPostRef( final TestModel obj, final Object referenceData, final String referenceName,
		final Request request )
	{

	}

	@Override
	public boolean beforeDeleteRef( final TestModel obj, final Object referenceData, final String referenceName,
		final Request request )
	{
		return false;
	}

	@Override
	public void afterDeleteRef( final TestModel obj, final Object referenceData, final String referenceName,
		final Request request )
	{

	}

	@Override
	public <Z extends AbstractClientDataModel> List<Z> afterGetAllReferences(
		final List<Z> listOfReferences,
		final String query,
		final String referencename,
		final Request request )
	{
		return null;
	}
}
