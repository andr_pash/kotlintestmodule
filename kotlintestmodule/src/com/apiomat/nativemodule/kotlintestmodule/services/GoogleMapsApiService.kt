package com.apiomat.nativemodule.kotlintestmodule.services

import okhttp3.OkHttpClient
import okhttp3.Request
import javax.ws.rs.core.UriBuilder

data class Coordinate(val lat: Double, val long: Double) {
    override fun toString(): String {
        return "$lat,$long"
    }
}

class GoogleMapsApiService(val httpClient: OkHttpClient) {
    val API_KEY = "AIzaSyAJZS0yA8HDhKf9djJMouiy_RcCf7gpgII"
    val baseUrl = "https://maps.googleapis.com/maps/api/staticmap"


    fun fetchImageForCoordinates(coordinate: Coordinate): ByteArray? {
        val center = coordinate.toString()

        val url = UriBuilder.fromPath(baseUrl)
                .queryParam("center", center)
                .queryParam("key", API_KEY)
                .queryParam("zoom", 13)
                .queryParam("mapType", "roadmap")
                .queryParam("size", "600x300")
                .build()
                .toURL()

        val getRequest = Request.Builder().url(url).get()

        val response = httpClient.newCall(getRequest.build()).execute()
        return response.body()?.bytes()
    }
}