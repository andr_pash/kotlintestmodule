package com.apiomat.nativemodule.kotlintestmodule.services

import com.apiomat.nativemodule.helpers.IModelHelper
import com.apiomat.nativemodule.kotlintestmodule.TestClazz
import com.apiomat.nativemodule.kotlintestmodule.TestModel
import java.io.ByteArrayInputStream

interface ITestService {
    fun log()
    fun makeCorrespondingTestClazzInstance(obj: TestModel)
    fun fetchImageForCoordinatesAndAttachToModel(obj: TestModel)
}

class TestService(private val modelHelper: IModelHelper, private val mapsApiService: GoogleMapsApiService) : ITestService {
    override fun log() {
        modelHelper.log("test log from kotlin")
    }

    override fun makeCorrespondingTestClazzInstance(obj: TestModel) {
        val testClazz = this.modelHelper.createModel(TestClazz::class.java)
        testClazz.foreignId = obj.id

        testClazz.save()
    }

    override fun fetchImageForCoordinatesAndAttachToModel(obj: TestModel) {
        modelHelper.log("trying to attach image")

        val image = mapsApiService.fetchImageForCoordinates(Coordinate(51.0, 12.0))

        if (image != null) {
            obj.postSomePicture(ByteArrayInputStream(image), "some_maps", "png")
            modelHelper.log("did attach image with size ${image.size}")
            obj.save()
        } else {
            modelHelper.log("no image received to attach")
        }
    }


}